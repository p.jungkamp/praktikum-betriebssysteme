//! This test test the Priority scheduler.
//!
//! It is based on cooperative scheduling, as we don't enable interrupts here.

#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(edu_kernel::test_runner)]

extern crate alloc;

#[macro_use]
extern crate edu_kernel;

use alloc::{boxed::Box, vec, vec::Vec};
use edu_kernel::scheduler::{
	self, priority_scheduler::PriorityScheduler, task::Priority, thread, TaskManager,
};
use edu_kernel::{arch::interrupts, logger};

/// This vector acts as a log of the order of the exection of the threads
static mut THREADVEC: Vec<u8> = Vec::new();

// This is a cooperative task, that pushes it's id into the THREADVEC
fn push_vec(id: u8) {
	for _ in 0..3 {
		// This is safe, as we don't have interrupts enabled. An Spinlock around
		// THREADVEC would also be fine.
		unsafe { THREADVEC.push(id) };

		// reschedule
		TaskManager::yield_now(scheduler::get())
	}
}

#[no_mangle]
/// A custom start function, as we want to modify the scheduler.
extern "C" fn start() -> ! {
	println!("Cooperative FiFo Scheduler Test");

	// The Logger needs to be enabled for any kind of Kernel Message
	logger::init().unwrap();
	unsafe { edu_kernel::arch::init() };
	edu_kernel::mm::init();
	interrupts::enable();

	// initalize the scheduling with a fifo scheduler
	let sched = Box::new(PriorityScheduler::new());
	edu_kernel::scheduler::custom_init(sched);

	// Spawn three threads
	let mut threads = Vec::new();
	threads.push(thread::spawn_prio(move || push_vec(0), Priority::NORMAL));
	threads.push(thread::spawn_prio(move || push_vec(1), Priority::LOW));
	threads.push(thread::spawn_prio(move || push_vec(2), Priority::HIGH));

	// wait for all threads to finish
	for t in threads {
		t.join();
	}

	assert_eq!(unsafe { &THREADVEC }, &vec![2, 2, 2, 0, 0, 0, 1, 1, 1]);


	edu_kernel::arch::processor::exit(0)
}
