#![no_std]
#![no_main]
#![feature(asm)]
#![feature(naked_functions)]
#![feature(custom_test_frameworks)]
#![test_runner(edu_kernel::test_runner)]

extern crate alloc;

#[macro_use]
extern crate edu_kernel;

use alloc::boxed::Box;
use alloc::vec;
use edu_kernel::logger;

#[no_mangle]
extern "C" fn start() -> ! {
	println!("Memory Test");

	// The Logger needs to be enabled for any kind of Kernel Message
	logger::init().unwrap();
	unsafe { edu_kernel::arch::init() };
	println!("initializing Memory System");
	edu_kernel::mm::init();

	// Do some allocations and deallocations
	let mut boxed_val = Box::new(42);
	assert_eq!(42, *boxed_val);
	*boxed_val += 1;
	assert_eq!(43, *boxed_val);
	drop(boxed_val);

	let mut v = vec![42; 1024];
	assert_eq!(42, v[1023]);
	v.push(5);
	assert_eq!(5, v[1024]);
	v.reserve(100);
	v.shrink_to_fit(); // do some deallocation
	drop(v);

	edu_kernel::arch::processor::exit(0)
}
