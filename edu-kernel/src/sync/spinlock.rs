//! Spinlock-based mutexes.
//!
//! These mutexes are used to share data across multiple cores.

use super::{Mutex, RawMutex};
use core::{
	hint,
	sync::atomic::{AtomicUsize, Ordering},
};

/// A raw [`Spinlock`].
///
/// Use [`Spinlock`] instead.
#[derive(Debug, Default)]
pub struct RawSpinlock {
	/// The next ticket to be handed out.
	next_ticket: AtomicUsize,
	/// The ticket which is currently being served.
	now_serving: AtomicUsize,
}

unsafe impl RawMutex for RawSpinlock {
	#[allow(clippy::declare_interior_mutable_const)]
	const INIT: Self = Self {
		next_ticket: AtomicUsize::new(0),
		now_serving: AtomicUsize::new(0),
	};

	fn lock(&self) {
		let ticket = self.next_ticket.fetch_add(1, Ordering::Relaxed);

		while self.now_serving.load(Ordering::Acquire) != ticket {
			hint::spin_loop()
		}
	}

	unsafe fn unlock(&self) {
		self.now_serving.fetch_add(1, Ordering::Release);
	}
}

/// A [ticket-lock]-based completely fair mutex.
///
/// [ticket-lock]: https://en.wikipedia.org/wiki/Ticket_lock
pub type Spinlock<T> = Mutex<RawSpinlock, T>;
