//! Interrupt-disabling mutexes.
//!
//! These mutexes are used for sharing data with interrupt handlers.

use super::{Mutex, RawMutex};
use crate::arch::interrupts;
use core::{
	hint,
	sync::atomic::{AtomicBool, Ordering},
};

/// A raw [`SingleCoreInterruptMutex`].
///
/// Use [`SingleCoreInterruptMutex`] instead.
#[derive(Debug, Default)]
pub struct RawSingleCoreInterruptMutex {
	/// Whether the interrupts were enabled before locking.
	interrupts_were_enabled: AtomicBool,
	lock: AtomicBool,
}

unsafe impl RawMutex for RawSingleCoreInterruptMutex {
	#[allow(clippy::declare_interior_mutable_const)]
	const INIT: Self = Self {
		interrupts_were_enabled: AtomicBool::new(false),
		lock: AtomicBool::new(false),
	};

	fn lock(&self) {
		let interrupts_were_enabled = interrupts::are_enabled();

		if interrupts_were_enabled {
			interrupts::disable()
		}

		self.interrupts_were_enabled
			.store(interrupts_were_enabled, Ordering::Relaxed);

		assert!(
			self.lock
				.compare_exchange_weak(false, true, Ordering::Acquire, Ordering::Relaxed)
				.is_ok(),
			"SingleCoreInterruptMutex is already locked"
		);
	}

	unsafe fn unlock(&self) {
		self.lock.swap(false, Ordering::Release);
		if self.interrupts_were_enabled.load(Ordering::Relaxed) {
			interrupts::enable()
		}
	}
}

/// A *single-core* interrupt-disabling mutex.
///
/// This mutex disables interrupts on the current core. Thus, once locked, the
/// current context cannot be preempted. This can be used to share core-local
/// data with core-local interrupts.
///
/// # Safety
///
/// This mutex is only safe for data accessed by a *single-core*.
pub type SingleCoreInterruptMutex<T> = Mutex<RawSingleCoreInterruptMutex, T>;

/// A raw [`InterruptSpinlock`].
///
/// Use [`InterruptSpinlock`] instead.
#[derive(Debug, Default)]
pub struct RawInterruptSpinlock {
	/// Whether the mutex is currently locked.
	lock: AtomicBool,
	/// Whether the interrupts were enabled before locking.
	interrupts_were_enabled: AtomicBool,
}

unsafe impl RawMutex for RawInterruptSpinlock {
	#[allow(clippy::declare_interior_mutable_const)]
	const INIT: Self = Self {
		lock: AtomicBool::new(false),
		interrupts_were_enabled: AtomicBool::new(false),
	};

	fn lock(&self) {
		let interrupts_were_enabled = interrupts::are_enabled();
		if interrupts_were_enabled {
			interrupts::disable()
		}

		while self
			.lock
			.compare_exchange_weak(false, true, Ordering::Acquire, Ordering::Relaxed)
			.is_err()
		{
			if interrupts_were_enabled {
				interrupts::enable()
			}

			hint::spin_loop();

			if interrupts_were_enabled {
				interrupts::disable()
			}
		}

		self.interrupts_were_enabled
			.store(interrupts_were_enabled, Ordering::Relaxed)
	}

	unsafe fn unlock(&self) {
		let interrupts_were_enabled = self.interrupts_were_enabled.swap(false, Ordering::Relaxed);

		self.lock.store(false, Ordering::Release);

		if interrupts_were_enabled {
			interrupts::enable()
		}
	}
}

/// A spinlock-based interrupt-disabling mutex.
///
/// It is a hybrid of an [`SingleCoreInterruptMutex`] and a [`Spinlock`]. This
/// can be used to share data with interrupts and other cores. To avoid
/// deadlocks when failing to aquire the lock, this is not a ticket lock, but a
/// boolean spinlock.
///
/// [`Spinlock`]: super::spinlock::Spinlock
pub type InterruptSpinlock<T> = Mutex<RawInterruptSpinlock, T>;
