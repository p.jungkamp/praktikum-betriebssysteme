//! Address manipulation.

use core::fmt;
use core::ops::{Add, AddAssign, BitAnd, BitOr, Div, Sub, SubAssign};

pub trait Address:
	Add + Sub + AddAssign + SubAssign + Copy + Sized + From<usize> + Into<usize>
{
	fn align_down(&self, align: usize) -> Self {
		Self::from(align_down((*self).into(), align))
	}

	fn align_up(&self, align: usize) -> Self {
		Self::from(align_up((*self).into(), align))
	}

	fn is_aligned(&self, align: usize) -> bool {
		is_aligned((*self).into(), align)
	}
}

// We should be able to modify an Address with a `usize`. Therefore we implement
// the Add and Sub traits for this

/// This Struct represents a physical address. It follows the newtype pattern
/// and encapsulates a `usize`. It usually can't be transmuted for
/// datastructure access, because an architecture which uses paging does not
/// allow direct physical memory access.
#[derive(
	Clone,
	Copy,
	Debug,
	Eq,
	Hash,
	Ord,
	PartialEq,
	PartialOrd,
	Add,
	Sub,
	AddAssign,
	SubAssign,
	LowerHex,
	UpperHex,
)]
pub struct PhysicalAddress(pub usize);
impl Add<usize> for PhysicalAddress {
	type Output = PhysicalAddress;
	fn add(self, other: usize) -> Self {
		Self(self.0 + other)
	}
}
impl AddAssign<usize> for PhysicalAddress {
	fn add_assign(&mut self, other: usize) {
		*self = PhysicalAddress(self.0 + other)
	}
}
impl Sub<usize> for PhysicalAddress {
	type Output = PhysicalAddress;
	fn sub(self, other: usize) -> Self {
		Self(self.0 - other)
	}
}
impl SubAssign<usize> for PhysicalAddress {
	fn sub_assign(&mut self, other: usize) {
		*self = PhysicalAddress(self.0 - other)
	}
}
impl BitAnd<usize> for PhysicalAddress {
	type Output = PhysicalAddress;
	fn bitand(self, other: usize) -> Self {
		Self(self.0 & other)
	}
}
impl BitOr<usize> for PhysicalAddress {
	type Output = PhysicalAddress;
	fn bitor(self, other: usize) -> Self {
		Self(self.0 | other)
	}
}
impl From<usize> for PhysicalAddress {
	fn from(u: usize) -> Self {
		PhysicalAddress(u)
	}
}
impl From<PhysicalAddress> for usize {
	fn from(p: PhysicalAddress) -> Self {
		p.0
	}
}
impl fmt::Pointer for PhysicalAddress {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{:p}", self.0 as *const u8)
	}
}
impl Address for PhysicalAddress {}

/// This Struct represents a virtual address. It follows the newtype pattern and
/// encapsulates a `usize`. This can be transmuted to access raw memory.
#[derive(Clone, Copy, Eq, Hash, Ord, PartialEq, PartialOrd, Add, Sub, AddAssign, SubAssign)]
pub struct VirtualAddress(pub usize);
impl Add<usize> for VirtualAddress {
	type Output = VirtualAddress;
	fn add(self, other: usize) -> Self {
		Self(self.0 + other)
	}
}
impl AddAssign<usize> for VirtualAddress {
	fn add_assign(&mut self, other: usize) {
		*self = VirtualAddress(self.0 + other)
	}
}
impl Sub<usize> for VirtualAddress {
	type Output = VirtualAddress;
	fn sub(self, other: usize) -> Self {
		Self(self.0 - other)
	}
}
impl SubAssign<usize> for VirtualAddress {
	fn sub_assign(&mut self, other: usize) {
		*self = VirtualAddress(self.0 - other)
	}
}
impl Div<usize> for VirtualAddress {
	type Output = VirtualAddress;
	fn div(self, other: usize) -> Self {
		Self(self.0 / other)
	}
}
impl BitAnd<usize> for VirtualAddress {
	type Output = VirtualAddress;
	fn bitand(self, other: usize) -> Self {
		Self(self.0 & other)
	}
}
impl From<usize> for VirtualAddress {
	fn from(u: usize) -> Self {
		VirtualAddress(u)
	}
}
impl From<VirtualAddress> for usize {
	fn from(v: VirtualAddress) -> Self {
		v.0
	}
}
impl fmt::Pointer for VirtualAddress {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{:p}", self.0 as *const u8)
	}
}
impl fmt::Debug for VirtualAddress {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "VirtAddr({:p})", self.0 as *const u8)
	}
}
impl Address for VirtualAddress {}

/// Align address downwards.
///
/// Returns the greatest x with alignment `align` so that x <= addr. The
/// alignment must be  a power of 2.
///
/// Taken from [`x86_64::addr`].
#[inline]
pub const fn align_down(addr: usize, align: usize) -> usize {
	assert!(align.is_power_of_two(), "`align` must be a power of two");
	addr & !(align - 1)
}

/// Align address upwards.
///
/// Returns the smallest x with alignment `align` so that x >= addr. The
/// alignment must be a power of 2.
///
/// Taken from [`x86_64::addr`].
#[inline]
pub const fn align_up(addr: usize, align: usize) -> usize {
	assert!(align.is_power_of_two(), "`align` must be a power of two");
	let align_mask = align - 1;
	if addr & align_mask == 0 {
		addr // already aligned
	} else {
		(addr | align_mask) + 1
	}
}

#[inline]
pub const fn is_aligned(addr: usize, align: usize) -> bool {
	assert!(align.is_power_of_two(), "`align` must be a power of two");
	let align_mask = align - 1;
	addr & align_mask == 0
}
