//! Macros that require their context to run only once.

/// Macro to that ensures its context is only executed once.
///
/// Returns a result, that may be unwrapped to panic on second call.
#[macro_export]
macro_rules! run_once {
	() => {{
		use core::result::Result;
		use core::sync::atomic::{AtomicBool, Ordering};

		static RAN: AtomicBool = AtomicBool::new(false);

		let res: Result<(), &'static str> =
			match RAN.compare_exchange(false, true, Ordering::Acquire, Ordering::Relaxed) {
				Ok(_) => Ok(()),
				Err(_) => Err("run_once ran twice"),
			};

		res
	}};
}

/// Macro to create a mutable reference to a statically allocated value.
///
/// The context of an invocation of this macro may only be executed once. On
/// further executions the macro returns an error.
///
/// Similar to `cortex_m::singleton`.
#[macro_export]
macro_rules! singleton {
	(: $ty:ty = $expr:expr) => {{
		use core::option::Option;
		use core::result::Result;

		let res: Result<&'static mut $ty, &'static str> = $crate::run_once!().map(|()| {
			static mut VAR: Option<$ty> = None;

			let expr = $expr;
			#[allow(unused_unsafe)]
			unsafe {
				VAR = Some(expr);
				VAR.as_mut().unwrap()
			}
		});

		res
	}};
}


#[cfg(test)]
mod tests {
	#[test]
	fn test_run_once() {
		fn run_once() -> Result<(), &'static str> {
			run_once!()
		}

		assert!(matches!(run_once(), Ok(_)));
		assert!(matches!(run_once(), Err(_)));
	}

	#[test]
	fn test_singleton() {
		let singleton = || singleton!(: usize = 0);

		let static_num: &'static mut usize = singleton().unwrap();

		assert_eq!(0, *static_num);
		assert!(matches!(singleton(), Err(_)));
	}
}
