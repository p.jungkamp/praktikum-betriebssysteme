//! Console output and colors.

use core::fmt;

/// Prints to the console.
#[macro_export]
macro_rules! print {
	($($arg:tt)*) => ({
		use core::fmt::Write;
		$crate::arch::CONSOLE.lock(|console| write!(console, $($arg)*).unwrap());
	});
}

/// Prints to the console, with a newline.
#[macro_export]
macro_rules! println {
	($($arg:tt)*) => ({
		use core::fmt::Write;
		$crate::arch::CONSOLE.lock(|console| writeln!(console, $($arg)*).unwrap());
	});
}

/// Terminal colors
///
/// Displaying this color affects the following text.
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub enum Color {
	Reset = 0,
	BlackFG = 30,
	RedFG,
	GreenFG,
	YellowFG,
	BlueFG,
	MagentaFG,
	CyanFG,
	WhiteFG,
	BlackBG = 40,
	RedBG,
	GreenBG,
	YellowBG,
	BlueBG,
	MagentaBG,
	CyanBG,
	WhiteBG,
	BrightBlackFG = 90,
	BrightRedFG,
	BrigthGreenFG,
	BrightYellowFG,
	BrightBlueFG,
	BrightMagentaFG,
	BrightCyanFG,
	BrightWhiteFG,
	BrightBlackBG = 100,
	BrightRedBG,
	BrigthGreenBG,
	BrightYellowBG,
	BrightBlueBG,
	BrightMagentaBG,
	BrightCyanBG,
	BrightWhiteBG,
}

impl fmt::Display for Color {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		// \x1b: ESC
		write!(f, "\x1b[{}m", *self as u8)
	}
}

#[cfg(test)]
mod tests {
	#[test]
	fn printtest() {
		print!("testprint");
		println!(" - testprintln");
	}
}
