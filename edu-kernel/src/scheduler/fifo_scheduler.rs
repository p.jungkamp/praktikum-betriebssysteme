//! FIFO-based scheduling
//!
//! All tasks are stored in a queue and the next task is simply the first in
//! line.

use super::{Scheduler, TaskRef};
use alloc::collections::VecDeque;

/// A FIFO-based scheduler.
#[derive(Debug, Default)]
pub struct FifoScheduler {
	/// The queue of tasks
	queue: VecDeque<TaskRef>,
}
impl FifoScheduler {
	/// Create a new Scheduler instance
	pub fn new() -> Self {
		FifoScheduler {
			queue: VecDeque::new(),
		}
	}
}

impl Scheduler for FifoScheduler {
	fn insert(&mut self, task: TaskRef) {
		self.queue.push_back(task)
	}

	fn next(&mut self) -> Option<TaskRef> {
		self.queue.pop_front()
	}
}
