//! Priority-based scheduling.

use crate::consts::NR_PRIORITIES;
use super::{Scheduler, TaskRef, Priority};
use alloc::collections::VecDeque;

/// A bitmap for priorities.
///
/// Supports getting the most significant bit.
#[derive(Copy, Clone, Default, Eq, PartialEq, Ord, PartialOrd, Hash)]
struct Bitmap(u32);

impl Bitmap {
	const BITS: u32 = u32::BITS;

	/// Returns the index of the most significant bit.
	const fn msb(self) -> Option<u32> {
		let leading_zeros = self.0.leading_zeros();
		if leading_zeros < Self::BITS {
			Some(Self::BITS - 1 - leading_zeros)
		} else {
			None
		}
	}

	fn insert(&mut self, bit: u32) {
		self.0 |= 0b1 << bit
	}

	fn remove(&mut self, bit: u32) {
		self.0 ^= 0b1 << bit
	}
}

/// A priority-based scheduler.
///
/// This scheduler uses a queue for each priority.
#[derive(Default)]
pub struct PriorityScheduler {
	/// One queue for each priority.
	queues: [VecDeque<TaskRef>; NR_PRIORITIES as usize],
	/// The priority bitmap.
	///
	/// This is used for getting the index of the highest available priority
	/// faster than by iterating over [`queues`](Self::queues).
	prio_bitmap: Bitmap,
}

impl PriorityScheduler {
	pub fn new() -> Self {
		Default::default()
	}

	fn highest_prio(&self) -> Option<Priority> {
		self.prio_bitmap
			.msb()
			.map(|prio| Priority::new(prio as u8).unwrap())
	}

	fn update_priority(&mut self, prio: Priority) {
		if self.queues[prio.get() as usize].is_empty() {
			self.prio_bitmap.remove(prio.get() as u32)
		} else {
			self.prio_bitmap.insert(prio.get() as u32)
		}
	}

	fn pop_from_queue(&mut self, prio: Priority) -> Option<TaskRef> {
		let task = self.queues[prio.get() as usize].pop_front();
		self.update_priority(prio);
		task
	}
}

impl Scheduler for PriorityScheduler {
	fn insert(&mut self, task: TaskRef) {
		let prio = task.lock(|task| task.prio());
		self.queues[prio.get() as usize].push_back(task);
		self.update_priority(prio);
	}

	fn next(&mut self) -> Option<TaskRef> {
		self.highest_prio()
			.map(|prio| self.pop_from_queue(prio).unwrap())
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_bitmap_msb() {
		assert_eq!(None, Bitmap(0).msb());
		assert_eq!(Some(29), Bitmap(u32::MAX >> 2).msb());
		assert_eq!(Some(31), Bitmap(u32::MAX).msb());
	}

	#[test]
	fn test_bitmap_bits() {
		assert!(NR_PRIORITIES as u32 <= Bitmap::BITS);
	}
}
