//! Lapic abstraction and init: Chapter 10.4 in Intel® 64 and IA-32 Architectures Software Developer’s Manual, Volume 3A

use core::{fmt::Debug, marker::PhantomData, ops::*};
use alloc::boxed::Box;
use log::trace;
use volatile::access::{ReadOnly, ReadWrite, Readable, Writable, WriteOnly};
use raw_cpuid::CpuId;
use x86::msr;

/// Abstraction providing volatile operations on memory mapped registers
#[repr(packed)]
pub struct LapicReg <T> {
    reg: u32,
    _pad: [u32;3],
    _permission: PhantomData<T>,
}

impl<T:Readable + Writable> LapicReg<T> {
    /// updates the value in Register 
    /// 
    /// performs volatile read; passes value to the closure; volatily writes return value of closure to reg 
    #[inline]
    pub unsafe fn update<F>(&mut self, func:F)
    where F: FnOnce(u32) -> u32 {
        let ptr = (&mut self.reg) as *mut u32;
        let val = ptr.read_volatile();
        ptr.write_volatile( func(val) );
    }
}

impl<T:Readable> LapicReg<T> {
    /// reads value from Register 
    /// 
    /// performs volatile read and returns value
    #[inline]
    pub unsafe fn read(&self) -> u32 {
        ((&self.reg) as *const u32).read_volatile()
    }
}

impl<T:Writable> LapicReg<T> {
    /// writes value to Register 
    /// 
    /// volatily write to reg 
    #[inline]
    pub unsafe fn write(&mut self, val:u32) {
        ((&mut self.reg) as *mut u32).write_volatile(val);
    }
}

/// Zero-Size-Struct 
///
///Signals LapicReg that no Operations are allowed on it
struct Reserved;

/// Non modifiable LapicReg
type LapicRegReserved = LapicReg<Reserved>;
/// read-write LapicReg
type RWLapicReg = LapicReg<ReadWrite>;
/// read-only LapicReg
type RLapicReg = LapicReg<ReadOnly>;
/// write-only LapicReg
type WLapicReg = LapicReg<WriteOnly>;

/// Representation of Memory mapped Lapic Registers
#[repr(packed)]
pub struct Lapic {
    _reserved0: [LapicRegReserved;2],
    /// LAPIC ID
    pub id: RWLapicReg,       
    /// LAPIC version
    pub version: RLapicReg, 
     
    _reserved1: [LapicRegReserved;4],
    /// Task priority
    pub tpr: RWLapicReg,      
    /// Arbitration priority
    pub apr: RLapicReg,      
    /// Processor priority 
    pub ppr: RLapicReg,      
    /// End of interrupt
    pub eoi: WLapicReg,     
    /// Remote read   
    pub rrd: RLapicReg,     
    /// Logical destination 
    pub ldr: RWLapicReg,     
    /// Destination format  
    pub dfr: RWLapicReg,     
    /// Spurious vector 
    pub svr: RWLapicReg,      
    /// In-service 
    pub isr: [RLapicReg;8],   
    /// Trigger mode
    pub tmr: [RLapicReg;8],   
    /// Interrupt request  
    pub irr: [RLapicReg;8],   
    /// Error status 
    pub esr: RLapicReg,      
    
    _reserved2: [LapicRegReserved;6],
    /// Corrected Machine Check vector 
    pub lvtcmci: RWLapicReg,      
    /// Interrupt command 
    pub icr: [RWLapicReg;2],    
    /// Timer vector 
    pub lvttimer: RWLapicReg,     
    /// Thermal sensor vector
    pub lvtts: RWLapicReg,         
    /// Perfmon counter vector
    pub lvtpmcr: RWLapicReg,      
    /// Local interrupt 0 vector  
    pub lvtlint0: RWLapicReg,     
    /// Local interrupt 1 vector 
    pub lvtlint1: RWLapicReg,    
    /// Error vector  
    pub lvterr: RWLapicReg,      
    /// Timer initial count
    pub timer_icr: RWLapicReg,    
    /// Timer current count 
    pub timer_ccr: RLapicReg,   
     
    _reserved3: [LapicRegReserved;4],
    /// Timer divide configuration
	pub timer_dcr: RWLapicReg,  
}

use core::fmt;
impl Debug for Lapic {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        unsafe { 
            f.debug_struct("Lapic")
                .field("id"        , &self.id.read())
                .field("version"   , &self.version.read())
                .field("tpr"       , &self.tpr.read())
                .field("apr"       , &self.apr.read())
                .field("ppr"       , &self.ppr.read())
                .field("rrd"       , &self.rrd.read())
                .field("ldr"       , &self.ldr.read())
                .field("dfr"       , &self.dfr.read())
                .field("svr"       , &self.svr.read())
                /*.field("isr"       , &self.isr.read())
                .field("tmr"       , &self.tmr.read())
                .field("irr"       , &self.irr.read())
                .field("esr"       , &self.esr.read())*/
                .field("lvtcmci"   , &self.lvtcmci.read())
                .field("icr [0]"   , &self.icr[0].read())
                .field("icr [0]"   , &self.icr[1].read())
                .field("lvttimer"  , &self.lvttimer.read())
                .field("lvtts"     , &self.lvtts.read())
                .field("lvtpmcr"   , &self.lvtpmcr.read())
                .field("lvtlint0"  , &self.lvtlint0.read())
                .field("lvtlint1"  , &self.lvtlint1.read())
                .field("lvterr"    , &self.lvterr.read())
                .field("timer_icr" , &self.timer_icr.read())
                .field("timer_ccr" , &self.timer_ccr.read())
                .field("timer_dcr" , &self.timer_dcr.read())
                .finish() 
        }
    }
}

/// Global pointer to the Lapic Memory Mapped Registers
///
/// Is Null before lapic::Lapic::init() call
pub static mut LAPIC: Option<Box<Lapic>> = None;

impl Lapic {
    /// get lapic_base from msr
    unsafe fn get_lapic_base_msr() -> u64 {
        trace!("Read IA32_APIC_BASE_MSR");
        msr::rdmsr(msr::IA32_APIC_BASE)
    }

    /// set lapic base msr to specified msr
    unsafe fn set_lapic_base_msr(msr: u64) {
        trace!("Write IA32_APIC_BASE_MSR: 0x{:016X}",msr);
        msr::wrmsr(msr::IA32_APIC_BASE,msr);
    }

    /// assert lapic is present; map lapic to global variable; set enable bit; allow all interrupt task priorities; clear spurious Interrupts
    pub unsafe fn init() {
        // check for feature support
        let features = 
            CpuId::new()
            .get_feature_info()
            .unwrap();

        // One should have an Apic if it is going to be initialized ¯\_(ツ)_/¯
        if !features.has_apic() { panic!("Go and get yourself an APIC!"); }

        trace!("initializing APIC");
        let mut msr = Self::get_lapic_base_msr();
        trace!("current msr entry: 0x{:X}",msr);

        // FIXME: Remove Relocation -----------------------------
        let new = core::mem::transmute::<&Lapic, u64>(&LAPIC_RELOCATED);
        msr = 
            if features.has_pae() { 
                msr.bitand(!0x_0000_000F_FFFF_F000)
              | new.bitand( 0x_0000_000F_FFFF_F000)
            } else {
                msr.bitand(!0x_0000_0000_FFFF_F000)
              | new.bitand( 0x_0000_0000_FFFF_F000)
            };
        // --------------------------------------------------------

        LAPIC = Some ( Box::from_raw(
            // Physical Address Extension determines whether low 4 bits of edx when reading msr are part of address
            if features.has_pae() { 
                msr.bitand(0x_0000_000F_FFFF_F000)
            } else {
                msr.bitand(0x_0000_0000_FFFF_F000)
            } as *mut Lapic
        ));

        Self::set_lapic_base_msr( msr );

        let lapic = LAPIC.as_mut().expect("Global Lapic address was NULL");

        // Enable LAPIC
        trace!("enable LAPIC; id 0x{:X}; version 0x{:X}", lapic.id.read(), lapic.version.read());
        lapic.svr
            .update(|v|
                v.bitor(0x1FFu32)
            );

        // allow all interrupt task priorities
        lapic.tpr
            .write( 0x00u32 );
        
        trace!("clear interrupts");
        // Clear up any spurious LAPIC interrupts
        lapic.notify_end_of_interrupt(0);
        trace!("Lapic initialized");
    }

    // notify end of interrupt
    unsafe fn notify_end_of_interrupt(&mut self, int_vec: u32) {
        self.eoi
            .write(int_vec);
    }
}


/// must be called from an interrupt by the Lapic to signal EOI
///
/// int_vec must be the valid interrupt vector of the interrupt it is called from
pub unsafe fn notify_end_of_interrupt(int_vec: u32) {
    LAPIC.as_mut().expect("Lapic not initialized").notify_end_of_interrupt(int_vec)
}


// FIXME: Remove Relocation ---------------------------

/// Sloppy thin Wrapper around Lapic Struct to ensure alignment of global Variable on Relocation
#[repr(align(4096))]
pub struct LapicAlignWrapper (Lapic);
impl Deref for LapicAlignWrapper {
    type Target = Lapic;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl DerefMut for LapicAlignWrapper {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

/// Global variable that the Apicbase will be remapped to
pub static mut LAPIC_RELOCATED: LapicAlignWrapper = LapicAlignWrapper( Lapic {
    _reserved0: [ 
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData}
    ],
    id: LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},       
    version: LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData}, 
    _reserved1: [ 
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData}
    ],
    tpr: LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},      
    apr: LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},      
    ppr: LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},      
    eoi: LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},     
    rrd: LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},     
    ldr: LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},     
    dfr: LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},     
    svr: LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},     
    isr: [
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData}
    ],   
    tmr: [
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData}
    ],   
    irr: [
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData}
    ],   
    esr: LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},         
    _reserved2: [
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData}
    ],
    lvtcmci: LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},      
    icr: [
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData}        
    ], 
    lvttimer: LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},     
    lvtts: LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},         
    lvtpmcr: LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},      
    lvtlint0: LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},     
    lvtlint1: LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},    
    lvterr: LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},      
    timer_icr: LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},    
    timer_ccr: LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},   
    _reserved3: [
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData},
        LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData}
    ],
    timer_dcr: LapicReg { reg:0, _pad: [0,0,0], _permission: PhantomData}
});