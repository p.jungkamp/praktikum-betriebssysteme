//! Abstractions for serial based I/O

use crate::sync::interrupt::InterruptSpinlock;
use core::fmt;
use x86_64::instructions::port::Port;

/// Data written to COM1 is forwarded by the hypervisor to its `Stdout`, which
/// is typically displayed in the hosts console.
pub static CONSOLE: InterruptSpinlock<Com> = InterruptSpinlock::new(Com::COM1);

/// A serial port (communication port, COM).
pub struct Com(Port<u8>);

impl Com {
	/// The first communication port, COM1.
	pub(crate) const COM1: Self = unsafe { Com::new_unchecked(0x3F8) };

	/// Creates a new serial port.
	///
	/// # Safety
	///
	/// The port must be a valid COM port.
	/// For more information see chapter 2 “Enhanced SPI Interface,” in the
	/// Intel® 495 Series Chipset Family On-Package Platform Controller Hub
	/// (PCH) Datasheet, Volume 2 of 2.
	const unsafe fn new_unchecked(port: u16) -> Self {
		Self(Port::new(port))
	}

	fn write(&mut self, value: u8) {
		// SAFETY: the port is guaranteed to be valid on construction
		unsafe { self.0.write(value) }
	}
}

/// Implementing this trait allows us to [`write!`] into [`Com`]s.
impl fmt::Write for Com {
	fn write_str(&mut self, s: &str) -> fmt::Result {
		for byte in s.bytes() {
			self.write(byte)
		}
		Ok(())
	}
}
