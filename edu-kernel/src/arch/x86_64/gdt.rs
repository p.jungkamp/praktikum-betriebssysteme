//! Configuration of the Global Descriptor Table (GDT).
//!
//! The [`GlobalDescriptorTable`] is used to switch between user and kernel mode
//! and to load a [`TaskStateSegment`].

use super::stack::Stack;
use crate::{scheduler, singleton};
use x86_64::{
	instructions::{segmentation, tables},
	structures::{
		gdt::{Descriptor, GlobalDescriptorTable, SegmentSelector},
		tss::TaskStateSegment,
	},
	PrivilegeLevel, VirtAddr,
};

static mut TSS: TaskStateSegment = TaskStateSegment::new();

#[repr(u16)]
pub enum StackIndex {
	NonMaskableInterrupt,
	DoubleFault,
	MachineCheck,
}
static mut INTERRUPT_STACKS: [Stack; 3] = [Stack::new(); 3];

pub const KERNEL_CODE_SEGMENT_SELECTOR: SegmentSelector =
	SegmentSelector::new(1, PrivilegeLevel::Ring0);
pub const KERNEL_DATA_SEGMENT_SELECTOR: SegmentSelector =
	SegmentSelector::new(2, PrivilegeLevel::Ring0);
pub const USER_CODE_SEGMENT_SELECTOR: SegmentSelector =
	SegmentSelector::new(3, PrivilegeLevel::Ring3);
pub const USER_DATA_SEGMENT_SELECTOR: SegmentSelector =
	SegmentSelector::new(4, PrivilegeLevel::Ring3);
const TSS_SEGMENT_SELECTOR: SegmentSelector = SegmentSelector::new(5, PrivilegeLevel::Ring0);

/// Initializes the GDT
///
/// # Safety
///
/// Must be called once at kernel initialization before starting the scheduler.
pub unsafe fn init() {
	let gdt = singleton!(: GlobalDescriptorTable = GlobalDescriptorTable::new()).unwrap();

	// Add GDT entries and verify constant segment selectors.
	assert_eq!(
		KERNEL_CODE_SEGMENT_SELECTOR,
		gdt.add_entry(Descriptor::kernel_code_segment())
	);
	assert_eq!(
		KERNEL_DATA_SEGMENT_SELECTOR,
		gdt.add_entry(Descriptor::kernel_data_segment())
	);

	assert_eq!(
		USER_CODE_SEGMENT_SELECTOR,
		gdt.add_entry(Descriptor::user_code_segment())
	);
	assert_eq!(
		USER_DATA_SEGMENT_SELECTOR,
		gdt.add_entry(Descriptor::user_data_segment())
	);

	let mut tss_descriptor = Descriptor::tss_segment(&TSS);
	// TODO: Remove hack increasing segment limit
	// Before the rewrite, the segment limit was calculated as base + size, which is
	// wrong. The calculation should not include base. However without base, it does
	// not work currently. I found the magic number 25 via trial and error.
	match &mut tss_descriptor {
		Descriptor::SystemSegment(value_low, _value_high) => *value_low += 25,
		_ => unreachable!(),
	}
	assert_eq!(TSS_SEGMENT_SELECTOR, gdt.add_entry(tss_descriptor));

	INTERRUPT_STACKS
		.iter()
		.zip(TSS.interrupt_stack_table.iter_mut())
		.for_each(|(stack, ist_entry)| *ist_entry = VirtAddr::new(stack.top() as u64));

	gdt.load();

	segmentation::load_ss(KERNEL_DATA_SEGMENT_SELECTOR);
	segmentation::set_cs(KERNEL_CODE_SEGMENT_SELECTOR);
}

/// Load the task state register with the first TSS entry.
pub fn register_task() {
	unsafe { tables::load_tss(TSS_SEGMENT_SELECTOR) }
}

pub unsafe extern "C" fn set_current_kernel_stack() {
	// TODO:
	// scheduler::switch_virtual_memory();

	// SAFETY: This is only called during a context switch, which is an
	// interrupt-free section
	let task_manager = &mut *scheduler::get().data_ptr();
	TSS.privilege_stack_table[0] = VirtAddr::new(task_manager.get_current_stack_top() as u64);
}
