//! Chapter 6, “Interrupt and Exception Handling,” in the Intel® 64 and IA-32
//! Architectures Software Developer’s Manual, Volume 3 (3A, 3B, 3C & 3D):
//! System Programming Guide

// Copyright (c) 2017-2018 Stefan Lankes, RWTH Aachen University
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

pub use x86_64::instructions::interrupts::{
	are_enabled, disable, enable, without_interrupts as free,
};

use crate::scheduler;
use crate::singleton;
#[allow(unused_imports)]
use log::{debug, trace, warn};
use super::lapic;
use x86_64::structures::idt::{InterruptDescriptorTable, InterruptStackFrame};

use super::gdt::StackIndex;

/// The interrupt offset of the first of the two [`PICS`].
const PIC_1_OFFSET: u8 = 32;

/// The interrupt offset of the second of the two [`PICS`].
const _PIC_2_OFFSET: u8 = PIC_1_OFFSET + 8;

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
#[repr(u8)]
/// An interrupt index for external interrupts, triggered by [`PICS`] and
/// starting at [`PIC_1_OFFSET`].
enum InterruptIndex {
	Timer = PIC_1_OFFSET,
}

/// The 8259 Programmable Interrupt Controller (PIC).
///
/// The PIC manages hardware interrupts and fires the appropriate interrupt
/// handler.
///
/// In protected mode, the first 32 interrupt vectors are reserved and used for
/// CPU exceptions, which conflicts with the PICs default real mode behaviour.
/// This is why we have to adjust the PIC's offsets (remap the PIC) accordingly
/// ([`PIC_1_OFFSET`], [`PIC_2_OFFSET`]) to use non-reserved interrupt vectors.
///
/// Using a lock here would create a race condition in EOIs, where we could be
/// interrupted after EOI and before unlocking the mutex again.

pub fn init() {
	//debug!("initialize interrupt descriptor table");

	let idt = singleton!(: InterruptDescriptorTable = InterruptDescriptorTable::new()).unwrap();
	unsafe {
		idt.non_maskable_interrupt
			.set_handler_fn(non_maskable_interrupt_handler)
			.set_stack_index(StackIndex::NonMaskableInterrupt as u16);
		idt.double_fault
			.set_handler_fn(double_fault_handler)
			.set_stack_index(StackIndex::DoubleFault as u16);
		idt.machine_check
			.set_handler_fn(machine_check_handler)
			.set_stack_index(StackIndex::MachineCheck as u16);
	}
	idt[InterruptIndex::Timer as usize].set_handler_fn(timer_interrupt_handler);
	set_unhandler_fns(idt);

	debug!("idt at {:p}", idt);
	idt.load();

	unsafe { lapic::Lapic::init() };
}

/// The [non maskable interrupt exception
/// (NMI)](InterruptDescriptorTable::non_maskable_interrupt) handler.
extern "x86-interrupt" fn non_maskable_interrupt_handler(stack_frame: &mut InterruptStackFrame) {
	panic!("Non maskable interrupt\nstack_frame = {:#?}", stack_frame)
}

/// The [double fault (`#DF`) exception](InterruptDescriptorTable::double_fault)
/// handler.
extern "x86-interrupt" fn double_fault_handler(
	stack_frame: &mut InterruptStackFrame,
	_error_code: u64,
) -> ! {
	panic!("Double fault\nstack_frame = {:#?}", stack_frame)
}

/// The [machine check exception
/// (`#MC`)(InterruptDescriptorTable::machine_check) handler.
extern "x86-interrupt" fn machine_check_handler(stack_frame: &mut InterruptStackFrame) -> ! {
	panic!("Machine check\nstack_frame = {:#?}", stack_frame)
}

/// The timer interrupt handler triggered by the [`System
/// Timer`](super::system_timer).
extern "x86-interrupt" fn timer_interrupt_handler(stack_frame: &mut InterruptStackFrame) {
	trace!("Timer Interrupt");
	trace!("stack_frame = {:#?}", stack_frame);
	// SAFETY: This cannot be inside an interrupt-free section
	unsafe {
		let task_manager = &mut *scheduler::get().data_ptr();
		task_manager.schedule();
	}
}

/// Signals the end of interrupt (EOI) for the timer interrupt handler
///
/// # Safety
///
/// Must be called last after finishing a potential context switch.
pub unsafe fn resume_preemption() {
	// Safety: On single core systems this is fine. External interrupts don't
	// interrupt other external interrupts with PICs.
	lapic::notify_end_of_interrupt(InterruptIndex::Timer as u32)
}

/// Default interrupt handlers
///
/// This macro implements default interrupt handlers, panicking with debug
/// information. Not implementing these handlers would cause a double
/// fault and prevent us from finding the causing exception.
///
/// The implemented handlers have to be registered using `set_unhandler_fns`.
macro_rules! unhandled_interrupts {
	(
		HandlerFunc: {$(
			$handler_func:ident,
		)+},
		HandlerFuncWithErrCode: {$(
			$handler_func_with_err_code:ident,
		)+},
	) => {
		fn set_unhandler_fns(idt: &mut InterruptDescriptorTable) {
			$(
				idt.$handler_func.set_handler_fn($handler_func);
			)+
			$(
				idt.$handler_func_with_err_code.set_handler_fn($handler_func_with_err_code);
			)+
		}

		$(
			extern "x86-interrupt" fn $handler_func(stack_frame: &mut InterruptStackFrame) {
				unimplemented!(
					"Interrupt handler \"{}\"\nstack_frame = {:#?}",
					stringify!($handler_func),
					stack_frame
				)
			}
		)+
		$(
			extern "x86-interrupt" fn $handler_func_with_err_code(stack_frame: &mut InterruptStackFrame, error_code: u64) {
				unimplemented!(
					concat!(
						"Interrupt handler \"{}\"\n",
						"error_code = {}\n",
						"stack_frame = {:#?}"
					),
					stringify!($handler_func_with_err_code),
					error_code,
					stack_frame
				)
			}
		)+
	};
}

unhandled_interrupts!(
	HandlerFunc: {
		divide_error,
		debug,
		breakpoint,
		overflow,
		bound_range_exceeded,
		invalid_opcode,
		device_not_available,
		x87_floating_point,
		simd_floating_point,
		virtualization,
	},
	HandlerFuncWithErrCode: {
		invalid_tss,
		segment_not_present,
		stack_segment_fault,
		general_protection_fault,
		alignment_check,
		security_exception,
	},
);
