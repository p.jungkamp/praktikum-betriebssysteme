// Copyright (c) 2017 Stefan Lankes, RWTH Aachen University
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

//! This module configures the 8284 programmable interval timer (PIT).

use crate::consts::TIMER_FREQ;
use core::sync::atomic::{AtomicBool, Ordering};
use log::{trace,debug,warn};
use super::lapic;
use core::ops::*;
use raw_cpuid::CpuId;
use core::convert::TryInto;

static SYSTEM_TIMER_INIT: AtomicBool = AtomicBool::new(false);
pub const TIMER_INT_VEC: u32 = 0x20u32;

/// The Lapic LVT programmable interval timer
///
/// This timer fires Interruptvector 32 periodically and is used for preemption.
struct LapicTimer {
	freq: u32
}

/// Global Value to store frequency after initialization
static mut LAPIC_TIMER: LapicTimer = LapicTimer { freq: 0 };

impl LapicTimer {
	/// The arbitrary (and probably wrong) counter frequency in Hz.

	unsafe fn write(cnt: u32, periodic: bool, active: bool) {
		let lapic = 
			lapic::LAPIC
			.as_mut()
			.expect("Lapic not initialized");

		// disable timer interrupt in lvttimer reg
		lapic.lvttimer
			.update( |r|
				r
				.bitor(1u32.shl(16u8))
			);
		
		// set divisor to 1
		lapic.timer_dcr
			.write( 0b1011u32 );

		// write periodic/active/int_vector mode
		lapic.lvttimer
			.update( |r|
				r
				.bitor(
					if periodic {0b1u32.shl(17u8)} else {0u32})
				.bitand(!0x10000u32)
				.bitor(
					if active {0u32} else {0x10000u32})
				.bitand(!0xFFu32)
				.bitor(TIMER_INT_VEC)
			);
		
		// write count 
		lapic.timer_icr
			.write( cnt );
	}

	/// Initialize the Timer.
	pub unsafe fn init(&mut self) {
		trace!("Initializing Lapic Timer");
		Self::write(0,false,false);

		let cpuid = CpuId::new();

		// determine frequency of lapic
		LAPIC_TIMER.freq = 
			cpuid
			.get_tsc_info()
			.and_then( |tscinfo| -> Option<u32> {
				let freq = tscinfo.nominal_frequency();
				if freq != 0 {
					Some(freq)
				} else {
					tscinfo
					.tsc_frequency()
					.and_then(  |f| 
						f.try_into().ok())
				}
			})
			.unwrap_or_else(|| { 
				warn!("Couldn't determine Lapic frequency from CPUID, using default");
				500_000_000
			}) as u32;

		debug!("Lapic frequency: {}Hz", LAPIC_TIMER.freq);
	}

	/// Start the system timer
	pub unsafe fn start(&self) {
		// Select a counter by writing a control word
		let cnt: u32 = (
			(self.freq + TIMER_FREQ/2)
			/ TIMER_FREQ
		) as u32;
		trace!("start lapic timer! Countdown: {}", cnt);
		Self::write( cnt, true, true );
	}

	/// Stop the system timer
	pub unsafe fn stop() {
		Self::write(0,false,false);
	}
}

/// Initialize the system timer
pub fn init() {
	SYSTEM_TIMER_INIT
		.compare_exchange(false, true, Ordering::SeqCst, Ordering::SeqCst)
		.expect("Timer initialized twice");
	unsafe { LAPIC_TIMER.init()};
}

/// Start the system timer
///
/// # Safety:
///
/// The timer must be initialized and have a correct handler. Otherwise this can
/// crash the system.
pub unsafe fn start() {
	if !SYSTEM_TIMER_INIT.load(Ordering::SeqCst) {
		panic!("Starting Timer before initialization");
	}
	LAPIC_TIMER.start();
}

/// Stop the system timer
///
/// # Safety:
///
/// This stops preemption and can thus lead to deadlocks.
pub unsafe fn stop() {
	if !SYSTEM_TIMER_INIT.load(Ordering::SeqCst) {
		panic!("Stopping system timer before initialization");
	}
	LapicTimer::stop();
}
