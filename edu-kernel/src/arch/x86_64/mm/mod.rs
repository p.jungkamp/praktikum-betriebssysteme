pub mod mem_size;
pub mod raw_mem_alloc;

use self::mem_size::{LargePageSize, PageSize};
use crate::{
	addr::{Address, VirtualAddress},
	run_once,
};
use core::num::NonZeroUsize;
use log::info;

/// Physical and virtual address of the first 2 MiB page that maps the kernel.
/// Obtained by the linker and aligned to page boundaries in the init function
/// Can be easily accessed through kernel_start_address()
///
/// [`Option`]`<`[`NonZeroUsize`]`>` avoids false initializations and utilizes
/// the fact that zero is an invalid value for memory layout opmitizations.
static mut KERNEL_START_ADDRESS: Option<NonZeroUsize> = None;

/// Physical and virtual address of the first page after the kernel.
/// Obtained by the linker and aligned to page boundaries in the init function
/// Can be easily accessed through kernel_end_address()
///
/// [`Option`]`<`[`NonZeroUsize`]`>` avoids false initializations and utilizes
/// the fact that zero is an invalid value for memory layout opmitizations.
static mut KERNEL_END_ADDRESS: Option<NonZeroUsize> = None;

/// These are just locations with no data behind it. They are set in the linker
/// script `eduos_remaster/util/x86_64.ld` before and after the kernel.
/// Therefore, the address of these values corresponds to the begining/end of
/// the sections.
extern "C" {
	static kernel_start_marker: u8;
	static kernel_end_marker: u8;
}

// these do not need to be pub
/// Returns the address of the first byte of the kernel code
pub fn kernel_start_address() -> VirtualAddress {
	unsafe { KERNEL_START_ADDRESS.unwrap().get().into() }
}

/// Returns the address of the first byte after the kernel code
pub fn kernel_end_address() -> VirtualAddress {
	unsafe { KERNEL_END_ADDRESS.unwrap().get().into() }
}

/// Returns the address of the first page of the kernel code
/// TODO: Return Page
pub fn kernel_start_page() -> VirtualAddress {
	kernel_start_address().align_down(LargePageSize::SIZE)
}

/// Returns the address of the first page after the the kernel code
/// TODO: Return Page
pub fn kernel_end_page() -> VirtualAddress {
	kernel_end_address().align_up(LargePageSize::SIZE)
}

/// Modify the kernel end marker.
unsafe fn modify_end_marker(offset: isize) {
	let default_kernel_end = &kernel_end_marker as *const u8 as usize;
	let kernel_end = if offset < 0 {
		default_kernel_end - offset.abs() as usize
	} else {
		default_kernel_end + offset.abs() as usize
	};
	KERNEL_END_ADDRESS = NonZeroUsize::new(kernel_end);
}

/// Initializes the memory management. Must only be called once! Panics on
/// second call.
pub unsafe fn init() {
	// Ensure that a second initialization fails
	run_once!().unwrap();

	KERNEL_START_ADDRESS = NonZeroUsize::new(&kernel_start_marker as *const u8 as usize);
	KERNEL_END_ADDRESS = NonZeroUsize::new(&kernel_end_marker as *const u8 as usize);

	info!("Kernel Memory System Initialized");
	info!("Memory size {} MByte", crate::arch::get_memory_size() >> 20);
	info!("Kernel start address 0x{:?}", kernel_start_address());
	info!("Kernel end address 0x{:?}", kernel_end_address());
}
