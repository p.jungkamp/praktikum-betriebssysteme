// ============================================================================
// ==== PAGE ==================================================================
// ============================================================================

/// A generic interface to support all possible page sizes.
///
/// This is defined as a subtrait of Copy to enable #[derive(Clone, Copy)] for
/// Page. Currently, deriving implementations for these traits only works if all
/// dependent types implement it as well.
pub trait PageSize: Copy {
	/// The page size in bytes.
	const SIZE: usize;

	/// For Debug purpose: The name
	const NAME: &'static str;
}

// TODO rename these to BasePage, LargePage...
/// A 4 KiB page mapped in the PT.
#[derive(Clone, Copy, Eq, PartialEq)]
pub enum BasePageSize {}
impl PageSize for BasePageSize {
	const SIZE: usize = 0x1000;
	const NAME: &'static str = "Base Page";
}

/// A 2 MiB page mapped in the PD.
#[derive(Clone, Copy, Eq, PartialEq)]
pub enum LargePageSize {}
impl PageSize for LargePageSize {
	const SIZE: usize = 0x200000;
	const NAME: &'static str = "Large Page";
}

/// A 1 GiB page mapped in the PDPT.
#[derive(Clone, Copy, Eq, PartialEq)]
pub enum HugePageSize {}
impl PageSize for HugePageSize {
	const SIZE: usize = 0x40000000;
	const NAME: &'static str = "Huge Page";
}

/// The size of a single Memory Frame
pub const FRAMESIZE: usize = 0x1000;
