//! This is a helper allocator to access the unused memory after the kernel

use super::{kernel_end_address, modify_end_marker};
use crate::addr::Address;
use crate::arch::x86_64::bootinfo::get_memory_size;
use crate::mm::Error;
use crate::sync::interrupt::InterruptSpinlock;
use core::mem::{align_of, size_of};
use core::slice::from_raw_parts_mut;

pub static RAW_ALLOCATOR: InterruptSpinlock<EarlyRawMemAllocator> =
	InterruptSpinlock::new(EarlyRawMemAllocator::new());

/// This Allocator utilizes the fact, that the memory after the kernel is unused
/// but mapped in memory. Allocations use this memory and push the kernel end
/// boundary back.
pub struct EarlyRawMemAllocator {
	/// We can use a normal bool, because the struct is encapsulated in a
	/// spinlock
	valid: bool,
}
impl EarlyRawMemAllocator {
	/// Creates the allocator. Because this is not `pub`, this is only called
	/// once.
	const fn new() -> Self {
		EarlyRawMemAllocator { valid: true }
	}

	/// Allocates some of the unused memory at boottime.
	///
	/// # Safety:
	///
	/// This is in principle safe, as long as [invalidate()](Self::invalidate)
	/// is called properly. Otherwise we this function might allocate memory
	/// which is already used elsewhere.
	pub unsafe fn allocate_region<T>(&mut self, count: usize) -> Result<&'static mut [T], Error> {
		if self.valid {
			let aligned_ptr = kernel_end_address().align_up(align_of::<T>());
			let alignment = aligned_ptr - kernel_end_address();

			// Increase the kernel end pointer, so that the memory won't get overwritten
			// This is fixed, as soon as the pagetables are created afterwards
			modify_end_marker((alignment.0 + size_of::<T>() * count) as isize);

			// Interpret that mem region as array of `T`
			Ok(from_raw_parts_mut::<'static, T>(
				aligned_ptr.0 as *mut T,
				count,
			))
		} else {
			Err(Error::InvalidAllocatorCalled)
		}
	}

	/// The remaining_memory in this allocator
	pub fn remaining_memory(&self) -> Result<usize, Error> {
		if self.valid {
			Ok(get_memory_size() - kernel_end_address().0)
		} else {
			Err(Error::InvalidAllocatorCalled)
		}
	}

	/// Invalidates the allocator, hence all following allocations will fail.
	/// Must be called before the memory after the kernel is used elsewhere.
	/// This is usually the case after the process' pagetables are created.
	pub fn invalidate(&mut self) {
		self.valid = false;
	}
}
