//! Timestamps and delays

use crate::scheduler::{self, TaskManager};
use core::arch::x86_64::_rdtsc;
use core::sync::atomic::{self, Ordering};

/// Returns the processor’s current time-stamp counter.
pub fn timestamp() -> u64 {
	atomic::fence(Ordering::SeqCst);
	let timestamp = unsafe { _rdtsc() };
	atomic::fence(Ordering::SeqCst);
	timestamp
}

/// Delays the execution for *at least* one I/O delay.
///
/// This can be used to wait long enough for an I/O port to process
/// configuration.
pub fn native_io_delay() {
	delay(1_000_000)
}

/// Delays the execution for *at least* `cycles`. Busy waiting.
pub fn delay(cycles: u64) {
	let start = timestamp();
	while timestamp() - start < cycles {}
}

/// Delays the execution for *at least* `cycles`. Switches to a differen task if
/// not ready yet.
pub fn delay_yielding(cycles: u64) {
	let start = timestamp();
	while timestamp() - start < cycles {
		log::debug!("yielding");
		TaskManager::yield_now(scheduler::get())
	}
}
