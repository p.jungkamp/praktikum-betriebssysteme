//! A FreeList is a linked list where each node represents a free memory
//! region.
//!
//! This FreeList implementation does not require external
//! datastructures, the nodes are located at the beginning of the free space
//! they indicate. If the memory is allocated, the node will simply be
//! part of the allocated space. This design without a separate datastructure
//! is inspried by the [FreeList from Phillip Opperman](https://os.phil-opp.com/allocator-designs/#implementation-1)
//!
//! Our FreeList is implemented in a way that we interpet the memory of the
//! freelist as an array of Nodes. This is not performance and
//! memory optimal, but it allows us to circumvent the borrow checker easily,
//! as we can comfortably work with indices. This results in the following
//! characteristics:
//!
//! - The first node is always reserved for a dummy HEAD node.
//! - The minimum possible allocation unit is the size of a node (24 bytes on
//!   x86_64)

use crate::addr::{Address, VirtualAddress};
use crate::sync::interrupt::InterruptSpinlock;
use core::alloc::{GlobalAlloc, Layout};
use core::{cmp::Ordering, fmt, mem::size_of, ops::Add, ops::Sub};
#[allow(unused_imports)]
use log::debug;

pub static FREELIST_MEMORY: InterruptSpinlock<Option<FreeList<'_>>> = InterruptSpinlock::new(None);
pub struct FreelistAllocator;
unsafe impl GlobalAlloc for FreelistAllocator {
	unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
		FREELIST_MEMORY.lock(|fl| {
			fl.as_mut()
				.expect("Allocation before FreelistAllocator was initialized")
				.allocate(layout)
				.unwrap()
				.0 as *mut u8
		})
	}

	unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
		FREELIST_MEMORY.lock(|fl| {
			fl.as_mut()
				.expect("Allocation before FreelistAllocator was initialized")
				.deallocate(VirtualAddress::from(ptr as usize), layout)
		})
	}
}

/// The allocation strategy of the FreeList
#[derive(Debug, Clone, Copy)]
pub enum AllocationStrategy {
	FirstFit,
	BestFit,
	WorstFit,
}

/// This is a node in the [FreeList](FreeList).
#[derive(Debug, PartialEq)]
struct Node {
	/// The number of FreeListNodes in this freelist
	nr_nodes: usize,
	next: Option<NodeIndex>,
}
impl Node {
	pub fn start_addr(&self) -> VirtualAddress {
		VirtualAddress(self as *const Self as usize)
	}
}

/// Size of a node. Alias for easier code.
const NODESIZE: usize = size_of::<Node>();

/// [Newtype](https://doc.rust-lang.org/rust-by-example/generics/new_types.html) for an index in the [FreeList].
#[derive(Debug, PartialEq, Copy, Clone)]
struct NodeIndex(usize);
impl Add<usize> for NodeIndex {
	type Output = NodeIndex;
	fn add(self, other: usize) -> Self {
		Self(self.0 + other)
	}
}
impl Sub<NodeIndex> for NodeIndex {
	type Output = NodeIndex;
	fn sub(self, other: NodeIndex) -> Self {
		Self(self.0 - other.0)
	}
}

/// divides `n` by `div` but always rounds up the result
fn div_round_up(n: usize, div: usize) -> usize {
	(n + div - 1) / div
}

/// The FreeList datastructure itself. See the [module documentation](self) for
/// more details.
pub struct FreeList<'a> {
	/// The Memory which is administrated by the freelist.
	memory: &'a mut [Node],
	/// The strategy by which a node is picked upon
	/// [allocation](FreeList::allocate).
	strategy: AllocationStrategy,
}
impl<'a> FreeList<'a> {
	/// Creates a new Bitmap. The whole content of mem is set to zero
	pub fn new(mem: &'a mut [u8]) -> Self {
		assert!(mem.len() > 2 * NODESIZE, "too little memory to operate on");

		let n_frames = mem.len() / NODESIZE;
		// Convert the memory slice into a list of nodes. This is safe, as long as we
		// ensure that every node is initialized prior to access.
		let frame_slice: &'a mut [Node] =
			unsafe { core::slice::from_raw_parts_mut(mem.as_mut_ptr() as *mut Node, n_frames) };

		//initialize the head
		frame_slice[0] = Node {
			nr_nodes: 0,
			next: Some(NodeIndex(1)),
		};
		// initialize the freespace
		frame_slice[1] = Node {
			nr_nodes: frame_slice.len() - 1,
			next: None,
		};

		Self {
			memory: frame_slice,
			strategy: AllocationStrategy::FirstFit,
		}
	}

	// Changes the strategy used by the freelist.
	pub fn set_strategy(&mut self, strategy: AllocationStrategy) {
		self.strategy = strategy;
	}

	/// Returns the start address of the memory.
	fn mem_start(&self) -> VirtualAddress {
		VirtualAddress(&self.memory[0] as *const _ as usize)
	}

	/// Returns the address of the first byte after the allocatable memory
	/// region. Debugging purpose only.
	fn mem_end(&self) -> VirtualAddress {
		VirtualAddress(&self.memory[self.memory.len() - 1] as *const _ as usize + NODESIZE)
	}

	/// Calculates the index of the given address
	fn get_index(&self, addr: VirtualAddress) -> Option<NodeIndex> {
		if addr >= self.mem_start() && addr < self.mem_end() {
			let node_ind = (addr - self.mem_start()).0 / NODESIZE;
			Some(NodeIndex(node_ind))
		} else {
			None
		}
	}

	/// Find a node's predecessor. node_ind must not be 0 which is the id of the
	/// HEAD.
	fn get_predecessor(&self, node_ind: NodeIndex) -> NodeIndex {
		assert_ne!(node_ind, NodeIndex(0));
		// Find the node's predecessor
		let mut prev_node_ind = NodeIndex(0);
		while let Some(cur_node_ind) = self.memory[prev_node_ind.0].next {
			if cur_node_ind == node_ind || cur_node_ind.0 > node_ind.0 {
				break;
			}
			prev_node_ind = self.memory[prev_node_ind.0].next.unwrap();
		}
		prev_node_ind
	}

	/// This function traverses the FreeList and searches for the first fitting
	/// memory region. Returns the index of the fitting node as well as the
	/// predecessor to that node and the number of nodes required to fulfil the
	/// layout requirement.
	fn find_node_first_fit(&self, layout: Layout) -> Option<(NodeIndex, NodeIndex, usize)> {
		let mut prev_node_ind = NodeIndex(0);
		while let Some(cur_node_ind) = self.memory[prev_node_ind.0].next {
			let cur_node_addr = self.memory[cur_node_ind.0].start_addr();
			let padding = (cur_node_addr.align_up(layout.align()) - cur_node_addr).0;
			// the minimum allocation size is the size of one free node. Otherwise we would
			// get a problem when deallocating this memory again
			let nr_nodes = div_round_up(layout.size() + padding, NODESIZE);

			if self.memory[cur_node_ind.0].nr_nodes >= nr_nodes {
				// we found a fitting node
				if padding >= NODESIZE {
					let padded_nodes = padding / NODESIZE;
					return Some((
						cur_node_ind + padded_nodes,
						cur_node_ind,
						nr_nodes - padded_nodes,
					));
				} else {
					return Some((cur_node_ind, prev_node_ind, nr_nodes));
				}
			} else {
				prev_node_ind = self.memory[prev_node_ind.0].next.unwrap();
			}
		}
		// List is full
		None
	}

	/// This function traverses the FreeList and searches for the best fitting
	/// memory region. Returns the index of that node and the index of it's
	/// predecessor.
	fn find_node_best_fit(&self, layout: Layout) -> Option<(NodeIndex, NodeIndex, usize)> {
		let mut prev_node_ind = NodeIndex(0);
		let mut best_gap = usize::MAX;
		let mut best_node_ind = None;
		while let Some(cur_node_ind) = self.memory[prev_node_ind.0].next {
			let cur_node_addr = self.memory[cur_node_ind.0].start_addr();
			let padding = (cur_node_addr.align_up(layout.align()) - cur_node_addr).0;
			// the minimum allocation size is the size of one free node. Otherwise we would
			// get a problem when deallocating this memory again
			let nr_nodes = div_round_up(layout.size() + padding, NODESIZE);
			let cur_node = &self.memory[cur_node_ind.0];
			if cur_node.nr_nodes >= nr_nodes && cur_node.nr_nodes < best_gap {
				// we found a better node
				if padding >= NODESIZE {
					let padded_nodes = padding / NODESIZE;
					best_gap = cur_node.nr_nodes - padded_nodes;
					best_node_ind = Some((
						cur_node_ind + padded_nodes,
						cur_node_ind,
						nr_nodes - padded_nodes,
					));
				} else {
					best_gap = cur_node.nr_nodes;
					best_node_ind = Some((cur_node_ind, prev_node_ind, nr_nodes));
				}
			}
			prev_node_ind = self.memory[prev_node_ind.0].next.unwrap();
		}
		best_node_ind
	}

	/// This function traverses the FreeList and searches for the best fitting
	/// memory region. Returns the index of that node and the index of it's
	/// predecessor.
	fn find_node_worst_fit(&self, layout: Layout) -> Option<(NodeIndex, NodeIndex, usize)> {
		let mut prev_node_ind = NodeIndex(0);
		let mut worst_gap = 0;
		let mut worst_node_ind = None;
		while let Some(cur_node_ind) = self.memory[prev_node_ind.0].next {
			let cur_node_addr = self.memory[cur_node_ind.0].start_addr();
			let padding = (cur_node_addr.align_up(layout.align()) - cur_node_addr).0;
			// the minimum allocation size is the size of one free node. Otherwise we would
			// get a problem when deallocating this memory again
			let nr_nodes = div_round_up(layout.size() + padding, NODESIZE);
			let cur_node = &self.memory[cur_node_ind.0];
			if cur_node.nr_nodes >= nr_nodes && cur_node.nr_nodes > worst_gap {
				// we found a better node
				if padding >= NODESIZE {
					let padded_nodes = padding / NODESIZE;
					worst_gap = cur_node.nr_nodes - padded_nodes;
					worst_node_ind = Some((
						cur_node_ind + padded_nodes,
						cur_node_ind,
						nr_nodes - padded_nodes,
					));
				} else {
					worst_gap = cur_node.nr_nodes;
					worst_node_ind = Some((cur_node_ind, prev_node_ind, nr_nodes));
				}
			}
			prev_node_ind = self.memory[prev_node_ind.0].next.unwrap();
		}
		worst_node_ind
	}

	/// Allocate memory of `size` bytes from the FreeList. Uses the allocation
	/// strategy configured by [set_strategy](FreeList::set_strategy).
	///
	/// This function is safe, but when used in an allocator, beware the
	/// following:
	/// - It must be ensured, that the FreeList lives longer than the allocated
	///   objects
	/// - It must be ensured, that the allocated objects are deallocated again.
	///   Otherwise we have a memory leadk
	pub fn allocate(&mut self, layout: Layout) -> Option<VirtualAddress> {
		let (node, pre_node, nr_nodes) = match self.strategy {
			AllocationStrategy::FirstFit => self.find_node_first_fit(layout)?,
			AllocationStrategy::BestFit => self.find_node_best_fit(layout)?,
			AllocationStrategy::WorstFit => self.find_node_worst_fit(layout)?,
		};

		unsafe { self.remove_node(node, pre_node, nr_nodes) };
		Some(self.memory[node.0].start_addr().align_up(layout.align()))
	}

	/// Wrapper for easier allocation code. Assumes an alignment of `1`. For
	/// testing purpose only.
	#[cfg(test)]
	fn allocate_unaligned(&mut self, size: usize) -> Option<VirtualAddress> {
		self.allocate(Layout::from_size_align(size, 1).unwrap())
	}


	/// Deallocates memory which was allocated in this FreeList.
	///
	/// # Safety
	///
	/// - The memory which is passed in `addr` must point to a valid memory
	///   region which was allocated from this list.
	/// If this is not the case, the whole list can break.
	/// - Memory must only be deallocated once. Double frees lead to invalid
	///   memory.
	/// - The memory at `addr` must not be used after allocation.
	pub unsafe fn deallocate(&mut self, addr: VirtualAddress, layout: Layout) {
		// the minimum allocation size is the size of one free node. Otherwise we would
		// get a problem when deallocating this memory again
		assert!(
			addr >= self.mem_start() && addr + layout.size() <= self.mem_end(),
			"Trying to deallocate memory that lies outside the freelists memory region"
		);
		let node_ind = self
			.get_index(addr)
			.expect("Trying to deallocate memory that lies outside the freelists memory region");
		let end_node_ind = self
			.get_index(addr + layout.size() - 1)
			.expect("Trying to deallocate memory that lies outside the freelists memory region");
		let prev_node_ind = self.get_predecessor(node_ind);
		self.insert_node(node_ind, prev_node_ind, 1 + end_node_ind.0 - node_ind.0);
	}

	/// Allocates space by removing a node. Also needs the index of the previous
	/// node. The node must be larger than `size + NODESIZE`.
	///
	/// # Safety
	///
	/// - The nodes must be valid. Otherwise this function would overwrite
	///   stuff. `nr_nodes` must not be larger than the number of actually free
	///   nodes at `node_ind`.
	unsafe fn remove_node(
		&mut self,
		node_ind: NodeIndex,
		prev_node_ind: NodeIndex,
		nr_nodes: usize,
	) {
		let node_distance = (node_ind - prev_node_ind).0;

		let prev_node = &self.memory[prev_node_ind.0];
		// Check if we're removing a potentially unallocated node in the middle of some
		// free space.
		if node_ind.0 < prev_node_ind.0 + prev_node.nr_nodes {
			// We do, so we need split the list at that point first
			self.memory[node_ind.0] = Node {
				nr_nodes: prev_node.nr_nodes - node_distance,
				next: prev_node.next,
			};
			self.memory[prev_node_ind.0] = Node {
				nr_nodes: node_distance,
				next: Some(node_ind),
			};
		}

		let cur_node = &self.memory[node_ind.0];
		let cur_node_len = cur_node.nr_nodes;
		let next_node_ind = match cur_node_len.cmp(&nr_nodes) {
			Ordering::Equal => {
				// The free space of this node fits the request perfectly
				cur_node.next
			}
			Ordering::Greater => {
				// We need to insert a new node after the requested free space
				let new_node_ind = node_ind + nr_nodes;
				self.memory[new_node_ind.0] = Node {
					nr_nodes: cur_node_len - nr_nodes,
					next: cur_node.next,
				};
				Some(new_node_ind)
			}
			Ordering::Less => {
				panic!("Tried to remove {} nodes at position {} from the freelist, but there are only {} nodes at this position", nr_nodes, node_ind.0, cur_node_len);
			}
		};

		// link the prev node to it's new successor
		self.memory[prev_node_ind.0].next = next_node_ind;
	}


	/// Insert a node and thus deallocate used space ("insert FreeSpace");
	///
	/// The deallocation function always keeps the list in order. This is
	/// required to merge consecutive free blocks
	fn insert_node(&mut self, node_ind: NodeIndex, prev_node_ind: NodeIndex, nr_nodes: usize) {
		assert!(nr_nodes > 0);
		assert!(node_ind.0 > prev_node_ind.0, "Nodes are not in order");

		let prev_node = &self.memory[prev_node_ind.0];

		assert!(
			prev_node
				.next
				.unwrap_or_else(|| self.get_index(self.mem_end() - 1).unwrap())
				.0 - prev_node_ind.0
				> nr_nodes,
			"nr of nodes cannot fit into this region"
		);

		// Amount of nodes in between prev_node and node
		let node_dist = node_ind.0 - prev_node_ind.0;

		// Insert the node. Return the node to check if it can be merged with the
		// succeeding one.
		let new_node_ind = if prev_node_ind != NodeIndex(0) && node_dist == prev_node.nr_nodes {
			// The node to deallocate comes directly after the current one  (and is not the
			// HEAD node) -> we can merge them.
			self.memory[prev_node_ind.0].nr_nodes += nr_nodes;
			prev_node_ind
		} else {
			// There is some allocated region in between. Create a new node
			self.memory[node_ind.0] = Node {
				nr_nodes,
				next: self.memory[prev_node_ind.0].next,
			};
			self.memory[prev_node_ind.0].next = Some(node_ind);
			node_ind
		};

		// Check if we can merge the succeeding node
		if let Some(next_node_ind) = self.memory[new_node_ind.0].next {
			// Amount of nodes in between next_node and node
			let next_node_dist = next_node_ind.0 - new_node_ind.0;
			if next_node_dist == self.memory[new_node_ind.0].nr_nodes {
				self.memory[new_node_ind.0].nr_nodes += self.memory[next_node_ind.0].nr_nodes;
				self.memory[new_node_ind.0].next = self.memory[next_node_ind.0].next;
			}
		}
	}

	#[allow(dead_code)]
	/// Counts the number of nodes in this FreeList. Debugging purpose mainly.
	fn nr_nodes(&self) -> usize {
		let mut count = 0;

		let mut prev_node_ind = NodeIndex(0);
		while let Some(cur_node_ind) = self.memory[prev_node_ind.0].next {
			count += 1;
			prev_node_ind = cur_node_ind;
		}
		count
	}

	#[cfg(test)]
	/// Traverses the list and checks for inconsistencies. This does not
	/// guarantee a correct list but it might find some errors.
	fn is_consistent(&self) -> bool {
		let mut prev_node_ind = NodeIndex(0);
		while let Some(cur_node_ind) = self.memory[prev_node_ind.0].next {
			if (cur_node_ind - prev_node_ind).0 < self.memory[prev_node_ind.0].nr_nodes {
				return false;
			}

			prev_node_ind = cur_node_ind;
		}
		true
	}

	#[cfg(test)]
	/// Returns the address of the first element in the list. Debugging purpose
	/// only.
	fn head(&self) -> Option<&Node> {
		if let Some(head_ind) = self.memory[0].next {
			Some(&self.memory[head_ind.0])
		} else {
			None
		}
	}

	#[cfg(test)]
	/// Returns the address of the first element in the list. Debugging purpose
	/// only.
	fn head_addr(&self) -> Option<VirtualAddress> {
		self.head().map_or(None, |n| Some(n.start_addr()))
	}
}
impl fmt::Debug for FreeList<'_> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		writeln!(
			f,
			"\nFreeList from {:p} to {:p}",
			self.mem_start(),
			self.mem_end()
		)?;

		let mut prev_node_ind = NodeIndex(0);
		while let Some(cur_node_ind) = self.memory[prev_node_ind.0].next {
			let cur_node = &self.memory[cur_node_ind.0];
			writeln!(
				f,
				" - node [{}]: Addr: {:?}, Nr of free nodes: {}",
				cur_node_ind.0,
				cur_node.start_addr(),
				cur_node.nr_nodes
			)?;
			prev_node_ind = cur_node_ind;
		}
		Ok(())
	}
}


#[cfg(test)]
mod test {
	use super::*;
	use crate::arch::FRAMESIZE;

	const MEMSIZE: usize = 10 * FRAMESIZE;

	#[repr(C)]
	#[repr(align(0x100))]
	struct Mem {
		mem: [u8; MEMSIZE],
	}

	#[test]
	fn freelist_basics() {
		let mut m = Mem { mem: [0; MEMSIZE] };
		let start_addr = VirtualAddress(&m as *const _ as usize);
		let fl = FreeList::new(&mut m.mem);
		assert_eq!(fl.memory.len(), MEMSIZE / NODESIZE);
		assert_eq!(fl.memory[1].nr_nodes, MEMSIZE / NODESIZE - 1);
		assert_eq!(fl.nr_nodes(), 1);
		assert_eq!(fl.mem_start(), start_addr);
		assert!(start_addr + MEMSIZE - fl.mem_end() < VirtualAddress(NODESIZE));
	}

	#[test]
	fn find_predecessor_test() {
		let mut m = Mem { mem: [0; MEMSIZE] };
		let mut list = FreeList::new(&mut m.mem);
		let _blk = list.allocate_unaligned(6 * NODESIZE).unwrap();
		// List: |h|x|x|x|x|x|x|n| | | | |
		// index:|0|1|2|3|4|5|6|7|8|9|
		assert_eq!(list.get_predecessor(NodeIndex(7)), NodeIndex(0));
		assert!(list.is_consistent());

		list.insert_node(NodeIndex(2), NodeIndex(0), 1);
		// List: |h|x|n|x|x|x|x|n| | | | |
		list.insert_node(NodeIndex(4), NodeIndex(2), 2);
		// List: |h|x|n|x|n| |x|n| | | | |

		assert_eq!(list.get_predecessor(NodeIndex(2)), NodeIndex(0));
		assert_eq!(list.get_predecessor(NodeIndex(4)), NodeIndex(2));
		assert!(list.is_consistent());

		list.insert_node(NodeIndex(3), NodeIndex(2), 1);
		// List: |h|x|n| | | |x|n| | | | |
		assert_eq!(list.get_predecessor(NodeIndex(7)), NodeIndex(2));
		assert!(list.is_consistent());
	}

	#[test]
	fn remove_node_test() {
		let mut m = Mem { mem: [0; MEMSIZE] };
		let mut list = FreeList::new(&mut m.mem);
		// List: |h|n| | | | | | | | |
		// index:|0|1|2|3|4|5|6|7|8|9|
		list.memory[1] = Node {
			nr_nodes: 1,
			next: Some(NodeIndex(3)),
		};
		list.memory[3] = Node {
			nr_nodes: 2,
			next: Some(NodeIndex(6)),
		};
		list.memory[6] = Node {
			nr_nodes: 20,
			next: None,
		};
		// List: |h|n|x|n| |x|n| | |

		assert_eq!(list.nr_nodes(), 3);
		assert!(list.is_consistent());
		unsafe { list.remove_node(NodeIndex(3), NodeIndex(1), 2) };
		assert_eq!(list.memory[1].next, Some(NodeIndex(6)));
		// List: |h|n| |x|x|x|n| | | |
		// index:|0|1|2|3|4|5|6|7|8|9|
		assert_eq!(list.nr_nodes(), 2);
		assert!(list.is_consistent());
		unsafe { list.remove_node(NodeIndex(6), NodeIndex(1), 2) };
		// List: |h|n| |x|x|x|x|x|n| |
		assert_eq!(list.memory[1].next, Some(NodeIndex(8)));
		assert_eq!(list.memory[8].next, None);
		assert_eq!(list.memory[8].nr_nodes, 20 - 2);
		assert_eq!(list.nr_nodes(), 2);
		assert!(list.is_consistent());
	}

	#[test]
	fn first_fit_allocation_test() {
		let mut m = Mem { mem: [0; MEMSIZE] };
		let start_addr = VirtualAddress(&m as *const _ as usize);
		let mut list = FreeList::new(&mut m.mem);
		list.set_strategy(AllocationStrategy::FirstFit);

		assert_eq!(list.allocate_unaligned(20).unwrap(), start_addr + NODESIZE);
		assert_eq!(
			list.head_addr().unwrap(),
			start_addr + (div_round_up(20, NODESIZE) + 1) * NODESIZE
		);
		assert!(list.is_consistent());

		assert_eq!(
			list.allocate_unaligned(128).unwrap(),
			start_addr + (div_round_up(20, NODESIZE) + 1) * NODESIZE
		);
		assert_eq!(
			list.head_addr().unwrap(),
			start_addr + (div_round_up(20, NODESIZE) + div_round_up(128, NODESIZE) + 1) * NODESIZE
		);
		assert!(list.is_consistent());

		assert_eq!(
			list.allocate_unaligned(1).unwrap(),
			start_addr + (div_round_up(20, NODESIZE) + div_round_up(128, NODESIZE) + 1) * NODESIZE
		);
		assert_eq!(list.allocate_unaligned(MEMSIZE), None);
		assert!(list.is_consistent());
	}

	#[test]
	fn small_allocation_test() {
		let mut m = Mem { mem: [0; MEMSIZE] };
		let start_addr = VirtualAddress(&m as *const _ as usize);
		let mut list = FreeList::new(&mut m.mem);
		list.set_strategy(AllocationStrategy::FirstFit);

		let _blk = list.allocate_unaligned(6 * NODESIZE).unwrap();
		// List: |h|x|x|x|x|x|x|n| | |
		// index:|0|1|2|3|4|5|6|7|8|9|

		list.insert_node(NodeIndex(1), NodeIndex(0), 1);
		// List: |h|n|x|x|x|x|x|n| | | |
		assert_eq!(list.nr_nodes(), 2);

		assert_eq!(list.allocate_unaligned(1), Some(start_addr + 1 * NODESIZE));
		assert_eq!(list.nr_nodes(), 1);
		// List: |x|x|x|x|x|x|n| | | | |
		assert!(list.is_consistent());

		assert_eq!(
			list.allocate_unaligned(1),
			Some(start_addr + (6 + 1) * NODESIZE)
		);
		assert_eq!(list.nr_nodes(), 1);
		// List: |x|x|x|x|x|x|x|n| | | |
		assert!(list.is_consistent());
	}

	#[test]
	fn insert_node_test_1() {
		let mut m = Mem { mem: [0; MEMSIZE] };
		let start_addr = VirtualAddress(&m as *const _ as usize);
		let mut list = FreeList::new(&mut m.mem);
		// List: |h|n| | | | | | | | |
		// index:|0|1|2|3|4|5|6|7|8|9|

		let blk = list.allocate_unaligned(6 * NODESIZE).unwrap();
		// List: |h||x|x|x|x|x|x|n| | | | |
		assert_eq!(blk, start_addr + 1 * NODESIZE);
		assert_eq!(list.head_addr().unwrap(), start_addr + (6 + 1) * NODESIZE);
		assert_eq!(list.nr_nodes(), 1);
		assert!(list.is_consistent());

		list.insert_node(NodeIndex(2), NodeIndex(0), 1);
		// List: |h|x|n|x|x|x|x|n| | | | |
		assert_eq!(list.head_addr().unwrap(), start_addr + 2 * NODESIZE);
		assert_eq!(list.nr_nodes(), 2);
		assert!(list.is_consistent());

		list.insert_node(NodeIndex(3), NodeIndex(2), 2);
		// List: |h|x|n| | |x|x|n| | | | |
		assert_eq!(list.head_addr().unwrap(), start_addr + 2 * NODESIZE);
		assert_eq!(list.nr_nodes(), 2);
		assert!(list.is_consistent());

		list.insert_node(NodeIndex(1), NodeIndex(0), 1);
		// List: |h|n| | | |x|x|n| | | | |
		assert_eq!(list.head_addr().unwrap(), start_addr + 1 * NODESIZE);
		assert_eq!(list.nr_nodes(), 2);
		assert!(list.is_consistent());
	}

	#[test]
	fn insert_node_test_2() {
		let mut m = Mem { mem: [0; MEMSIZE] };
		let start_addr = VirtualAddress(&m as *const _ as usize);
		let mut list = FreeList::new(&mut m.mem);
		// List: |n| | | | | | | | | |
		// index:|0|1|2|3|4|5|6|7|8|9|

		let blk = list.allocate_unaligned(6 * NODESIZE).unwrap();
		// List: |h||x|x|x|x|x|x| | | | | |
		assert_eq!(blk, start_addr + NODESIZE);
		assert_eq!(list.head_addr().unwrap(), start_addr + 7 * NODESIZE);
		assert_eq!(list.nr_nodes(), 1);
		assert!(list.is_consistent());

		list.insert_node(NodeIndex(2), NodeIndex(0), 1);
		// List: |h||x|n|x|x|x|x|n| | | | |
		assert_eq!(list.head_addr().unwrap(), start_addr + 2 * NODESIZE);
		assert_eq!(list.nr_nodes(), 2);
		assert!(list.is_consistent());

		list.insert_node(NodeIndex(4), NodeIndex(2), 1);
		// List: |h||x|n|x|n|x|x|n| | | | |
		assert_eq!(list.head_addr().unwrap(), start_addr + 2 * NODESIZE);
		assert_eq!(list.nr_nodes(), 3);
		assert!(list.is_consistent());

		list.insert_node(NodeIndex(3), NodeIndex(2), 1);
		// List: |h||x|n| | |x|x|n| | | | |
		assert_eq!(list.head_addr().unwrap(), start_addr + 2 * NODESIZE);
		assert_eq!(list.nr_nodes(), 2);
		assert!(list.is_consistent());
	}

	#[test]
	fn insert_node_test_3() {
		let mut m = Mem { mem: [0; MEMSIZE] };
		let start_addr = VirtualAddress(&m as *const _ as usize);
		let mut list = FreeList::new(&mut m.mem);
		// List: |h|n| | | | | | | | |
		// index:|0|1|2|3|4|5|6|7|8|9|

		let blk = list.allocate_unaligned(6 * NODESIZE).unwrap();
		// List: |h|x|x|x|x|x|x|n| | | | |
		assert_eq!(blk, start_addr + NODESIZE);
		assert_eq!(list.head_addr().unwrap(), start_addr + 7 * NODESIZE);

		assert_eq!(list.nr_nodes(), 1);
		assert!(list.is_consistent());

		list.insert_node(NodeIndex(2), NodeIndex(0), 1);
		// List: |h|x|n|x|x|x|x|n| | | | |
		assert_eq!(list.head_addr().unwrap(), start_addr + 2 * NODESIZE);
		assert_eq!(list.nr_nodes(), 2);
		assert!(list.is_consistent());

		list.insert_node(NodeIndex(4), NodeIndex(2), 1);
		// List: |h|x|n|x|n|x|x|n| | | | |
		assert_eq!(list.head_addr().unwrap(), start_addr + 2 * NODESIZE);
		assert_eq!(list.nr_nodes(), 3);
		assert!(list.is_consistent());

		list.insert_node(NodeIndex(3), NodeIndex(2), 1);
		// List: |h|x|n| | |x|x|n| | | | |
		assert_eq!(list.head_addr().unwrap(), start_addr + 2 * NODESIZE);
		assert_eq!(list.nr_nodes(), 2);
		assert!(list.is_consistent());
	}


	#[test]
	fn best_fit_test() {
		let mut m = Mem { mem: [0; MEMSIZE] };
		let start_addr = VirtualAddress(&m as *const _ as usize);
		let mut list = FreeList::new(&mut m.mem);
		list.set_strategy(AllocationStrategy::BestFit);
		// List: |h|n| | | | | | | | |
		// index:|0|1|2|3|4|5|6|7|8|9|

		let blk = list.allocate_unaligned(6 * NODESIZE).unwrap();
		// List: |h|x|x|x|x|x|x|n| | | | |
		assert_eq!(blk, start_addr + NODESIZE);
		assert_eq!(list.head_addr().unwrap(), start_addr + 7 * NODESIZE);

		assert_eq!(list.nr_nodes(), 1);
		assert!(list.is_consistent());

		list.insert_node(NodeIndex(2), NodeIndex(0), 2);
		list.insert_node(NodeIndex(5), NodeIndex(2), 1);
		// List: |h|x|n| |x|n|x|n| | | | |
		assert_eq!(list.head_addr().unwrap(), start_addr + 2 * NODESIZE);

		let blk = list.allocate_unaligned(NODESIZE).unwrap();
		// List: |h|x|n| |x|x|x|n| | | | |
		assert_eq!(blk, start_addr + 5 * NODESIZE);
		assert_eq!(list.head_addr().unwrap(), start_addr + 2 * NODESIZE);

		let blk = list.allocate_unaligned(3 * NODESIZE).unwrap();
		// List: |h|x|n| |x|x|x|x|x|x|n| |
		assert_eq!(blk, start_addr + 7 * NODESIZE);

		let blk = list.allocate_unaligned(2 * NODESIZE).unwrap();
		// List: |h|x|x|x|x|x|x|x|x|x|n| |
		assert_eq!(blk, start_addr + 2 * NODESIZE);
		assert!(list.is_consistent());
	}

	#[test]
	fn worst_fit_test() {
		let mut m = Mem { mem: [0; MEMSIZE] };
		let start_addr = VirtualAddress(&m as *const _ as usize);
		let mut list = FreeList::new(&mut m.mem);
		list.set_strategy(AllocationStrategy::WorstFit);
		assert!(list.is_consistent());
		// List: |h|n| | | | | | | | |
		// index:|0|1|2|3|4|5|6|7|8|9|

		let blk = list.allocate_unaligned(6 * NODESIZE).unwrap();
		// List: |h|x|x|x|x|x|x|n| | | | |
		assert_eq!(blk, start_addr + NODESIZE);
		assert_eq!(list.head_addr().unwrap(), start_addr + 7 * NODESIZE);

		assert_eq!(list.nr_nodes(), 1);

		list.insert_node(NodeIndex(2), NodeIndex(0), 1);
		list.insert_node(NodeIndex(4), NodeIndex(2), 2);
		// List: |h|x|n|x|n| |x|n| | | | |

		let blk = list.allocate_unaligned(NODESIZE).unwrap();
		// List: |h|x|n|x|n| |x|x|n| | | |
		assert_eq!(blk, start_addr + 7 * NODESIZE);
		assert_eq!(list.head_addr().unwrap(), start_addr + 2 * NODESIZE);
		assert_eq!(list.nr_nodes(), 3);

		let blk = list.allocate_unaligned(2 * NODESIZE).unwrap();
		// List: |h|x|n|x|n| |x|x|x|x|n| |
		assert_eq!(blk, start_addr + 8 * NODESIZE);
		assert_eq!(list.nr_nodes(), 3);

		let blk = list.allocate_unaligned(2 * NODESIZE).unwrap();
		// List: |h|x|x|x|x|x|x|n| | | | |
		assert_eq!(blk, start_addr + 10 * NODESIZE);
		assert_eq!(list.nr_nodes(), 3);
		assert!(list.is_consistent());
	}

	#[test]
	fn allocate_aligned_test() {
		for strat in [
			AllocationStrategy::FirstFit,
			AllocationStrategy::BestFit,
			AllocationStrategy::WorstFit,
		]
		.iter()
		{
			let mut m = Mem { mem: [0; MEMSIZE] };
			let start_addr = VirtualAddress(&m as *const _ as usize);
			let mut list = FreeList::new(&mut m.mem);
			list.set_strategy(*strat);
			// List: |h|n| | | | | | | | |
			// index:|0|1|2|3|4|5|6|7|8|9|

			assert!(NODESIZE < 32);
			assert!(NODESIZE > 16);

			let layout = Layout::from_size_align(3, 32).unwrap();
			let blk1 = list.allocate(layout).unwrap();
			// List: |h|x|n| | | | | | | |
			// index:|0|1|2|3|4|5|6|7|8|9|0|1|2|
			// blk:  | |1| | | | | | | | | | | |
			assert_eq!(blk1, (start_addr + NODESIZE).align_up(32));
			assert_eq!(list.head_addr().unwrap(), start_addr + 2 * NODESIZE);
			assert!(list.is_consistent());
			assert_eq!(list.nr_nodes(), 1);

			let layout = Layout::from_size_align(100, 64).unwrap();
			let blk2 = list.allocate(layout).unwrap();
			// List: |h|x|x|x|x|x|x|n| | | | |
			// index:|0|1|2|3|4|5|6|7|8|9|0|1|2|
			// blk:  | |1|2|2|2|2|2| | | | | | |
			assert_eq!(blk2, (start_addr + 2 * NODESIZE).align_up(64));
			assert_eq!(
				list.head_addr().unwrap(),
				start_addr + (2 + 100 / NODESIZE + 1) * NODESIZE
			);
			assert!(list.is_consistent());
			assert_eq!(list.nr_nodes(), 1);

			let layout = Layout::from_size_align(3, 64).unwrap();
			let blk3 = list.allocate(layout).unwrap();
			let blk4 = list.allocate(layout).unwrap();
			// index:|0|1|2|3|4|5|6|7|8|9|0|1|2|
			// blk:  | |1|2|2|2|2|2| |3| |4| | |
			assert_eq!((blk4 - blk3).0, 64);
			assert!(list.is_consistent());
			assert_eq!(list.nr_nodes(), 3);
		}
	}

	#[test]
	fn deallocate_aligned_test() {
		let mut m = Mem { mem: [0; MEMSIZE] };
		let mut list = FreeList::new(&mut m.mem);
		list.set_strategy(AllocationStrategy::FirstFit);

		assert_eq!(NODESIZE, 24);

		let layout1 = Layout::from_size_align(3, 32).unwrap();
		let blk1 = list.allocate(layout1).unwrap();
		let layout2 = Layout::from_size_align(100, 64).unwrap();
		let blk2 = list.allocate(layout2).unwrap();
		let layout3 = Layout::from_size_align(3, 64).unwrap();
		let blk3 = list.allocate(layout3).unwrap();
		let blk4 = list.allocate(layout3).unwrap();

		// index:|0|1|2|3|4|5|6|7|8|9|0|1|2|
		// blk:  | |1|2|2|2|2|2| |3| |4| | |

		assert_eq!(list.nr_nodes(), 3);
		unsafe { list.deallocate(blk1, layout1) };
		assert_eq!(list.nr_nodes(), 4);
		assert_eq!(list.memory[0].next, Some(NodeIndex(1)));
		assert!(list.is_consistent());

		// index:|0|1|2|3|4|5|6|7|8|9|0|1|2|
		// blk:  | | |2|2|2|2|2| |3| |4| | |

		unsafe { list.deallocate(blk3, layout3) };
		assert_eq!(list.nr_nodes(), 3);
		assert_eq!(list.memory[1].nr_nodes, 1);
		assert!(list.is_consistent());

		// index:|0|1|2|3|4|5|6|7|8|9|0|1|2|
		// blk:  | | |2|2|2|2|2| | | |4| | |

		unsafe { list.deallocate(blk2, layout2) };
		assert_eq!(list.nr_nodes(), 2);
		assert_eq!(list.memory[1].nr_nodes, 9);
		assert!(list.is_consistent());

		unsafe { list.deallocate(blk4, layout3) };
		assert_eq!(list.nr_nodes(), 1);
		assert!(list.is_consistent());
	}
}
