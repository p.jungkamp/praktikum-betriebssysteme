//! Thread functions from the kernel

use crate::{arch::syscall, syscall::Syscall};

/// Exits the current thread with the provided result.
pub fn exit(result: Result<(), ()>) -> ! {
	let code = match result {
		Ok(()) => 0,
		Err(()) => 1,
	};
	unsafe { syscall::syscall1(Syscall::Exit as _, code) };
	unreachable!();
}
