// Export our platform-specific modules.
// #[cfg(target_arch = "x86_64")]
// pub use self::x86_64::{
// 	get_memfile, get_memory_size, init, irq, jump_to_user_land, processor,
// register_task, serial, };

// "Activate" the x86_64 modules when the target architecture is x86_64
#[cfg(target_arch = "x86_64")]
pub mod x86_64;

#[cfg(target_arch = "x86_64")]
pub use self::x86_64::syscall;
