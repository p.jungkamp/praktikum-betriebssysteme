#![deny(rust_2018_idioms)]

use proc_macro::TokenStream;
use quote::{quote, quote_spanned};
use syn::{parse_macro_input, ItemFn};

/// This function attribute wraps `#[test_case]` and adds printing of status
/// information.
#[proc_macro_attribute]
pub fn test(_attr: TokenStream, input: TokenStream) -> TokenStream {
	let ItemFn {
		attrs,
		vis,
		sig,
		block,
	} = parse_macro_input!(input as ItemFn);

	let ident = sig.ident.to_string();
	let line = quote_spanned!(sig.ident.span()=> line!());

	let expanded = quote! {
		// Add `#[test_case] to the function
		#[test_case]
		#(#attrs)*
		#vis #sig {
			// Print the function name and location before the test
			println!("\u{1b}[0m\u{1b}[33;10m[Testing {:?} in {}:{}]\u{1b}[0m", #ident, file!(), #line);

			#block

			// Print a green `[Test sucessfull]` after the test
			println!("\u{1b}[0m\u{1b}[32;15m[Test sucessfull]\u{1b}[0m");
		}
	};

	expanded.into()
}
